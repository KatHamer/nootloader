; Nootloader
; Simple bootloader based on the MikeOS bootloader
; By Kat Hamer

; Set up the system into a basic useable state
bits 16     ; Run in 16-bit real mode
org 0x7c00  ; Run at this location in memory
jmp boot    ; Jump to boot function

; Define the floppy descriptor (Copied from the MikeOS source code)
OEMLabel		db "NOOTLOAD"	; Disk label
BytesPerSector		dw 512		; Bytes per sector
SectorsPerCluster	db 1		; Sectors per cluster
ReservedForBoot		dw 1		; Reserved sectors for boot record
NumberOfFats		db 2		; Number of copies of the FAT
RootDirEntries		dw 224		; Number of entries in root dir
					; (224 * 32 = 7168 = 14 sectors to read)
LogicalSectors		dw 2880		; Number of logical sectors
MediumByte		db 0F0h		; Medium descriptor byte
SectorsPerFat		dw 9		; Sectors per FAT
SectorsPerTrack		dw 18		; Sectors per track (36/cylinder)
Sides			dw 2		; Number of sides/heads
HiddenSectors		dd 0		; Number of hidden sectors
LargeSectors		dd 0		; Number of LBA sectors
DriveNo			dw 0		; Drive No: 0
Signature		db 41		; Drive signature: 41 for floppy
VolumeID		dd 00000000h	; Volume ID: any number
VolumeLabel		db "NOOTLOADER "; Volume Label: any 11 chars
FileSystem		db "FAT12   "	; File system type: don't change!

; Main boot function
boot:
  ; First, display a message to indicate that the boot process has started
  mov bp, bootmsg
  mov cx, bootmsg_len
  mov dh, 0
  mov dl, 0
  mov bl, white
  call print
   
  mov [bootdev], dl  ; Save boot device number
  mov ah, 8  ; BIOS call for getting drive parameters
  int 0x13  ; Call BIOS
  jc disk_error  ; Jump to disk error if BIOS reports one
  and cx, 3Fh  ; Get maximum sector number
  mov [SectorsPerTrack], cx  ; Sector numbers start at 1
  movzx  ; Get maximum head number
  add dx, 1  ; Head numbers start at 0, add 1 for total
  mov [Sides], dx

floppy_ok:
  mov ax, 19  ; Root dir starts at logical sector 19
  call 12hts

  mov si, buffer
  mov bx, ds
  mov es, bx
  mov bx, si

  mov ah, 2
  mov al, 14

  pusha

read_root_dir:
  popa
  pusha
  

  stc
  int 13h

  jnc search_dir
  call reset_floppy
  jnc read_root_dir

  jmp reboot

search_dir:
  popa

  mov ax, ds
  mov es, ax
  mov di, buffer

  mov cx, word [RootDirEntries]
  mov ax, 0

next_root_entry:
  xchg cx, dx

  mov si, loads
  mov cx, 11
  rep cmpsb
  je found_file_to_load

  add ax, 32

  mov di, buffer
  add di, ax

  xchg dx, cx
  loop next_root_entry

  mov bp, fileerrmsg
  mov cx, fileerrmsg_len
  mov dh, 0
  mov dl, 0
  mov bl, red
  call print


; Floppy controller
floppy_read:
  nop
    
; Print function
; IN: BP=MSG, CX=LEN, DH=XPOS, DL=YPOS, BL=COL
print:
  xor es, es   ; Set extra segment register to 0
  xor bh, bh   ; Set page number to 0
  mov al, 1    ; Set write mode to normal
  mov ah 0x13  ; BIOS routine for write string
  int 0x10     ; Call BIOS
  ret          ; Return

; Color definitions
black db 0
blue db 1
green db 2
cyan db 3
red db 4
magenta db 5
brown db 6
gray db 7
dark_gray db 8
light_blue db 9
light_green db 10
light_cyan db 11
light_red db 12
light_magenta db 13
yellow db 14
white db 15

; Boot info
loads dw "kernel.bin",0  ; File to load

; Miscellaneous data
bootmsg dw "Booting..."
bootmsg_len equ $-bootmsg

floppyerrmsg dw "Fatal floppy error!"
floppyerrmsg equ $-floppyerrmsg

fileerrmsg dw "Kernel not found!"
fileerrmsg equ $-fileerrmsg

; Boot sector information
times 510-($-$$) db 0  ; Pad remainder of boot sector with 0s
dw 0xAA55  ; Boot signature to tell BIOS that we are bootable code